package com.pajato.sicp

import java.lang.IllegalArgumentException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class MainTest {

   @Test fun `verify test environment`() { assertEquals(4, 2 + 2) }

   @Test fun `verify an invalid row throws illegal argument exception`() {
      assertFailsWith<IllegalArgumentException> { pascal(0, 1) }
   }

   @Test fun `verify an invalid column throws illegal argument exception`() {
      assertFailsWith<IllegalArgumentException> { pascal(1, 0) }
      assertFailsWith<IllegalArgumentException> { pascal(1, 2) }
   }

   @Test fun `verify first row value is 1`() { assertEquals(1, pascal(1, 1)) }

   @Test fun `verify both elements of the second row are 1`() {
      assertEquals(1, pascal(2, 1))
      assertEquals(1, pascal(2, 2))
   }

   //@ParameterizedTest
   //@MethodSource("generateTestCases")
   fun generateTestCases(): List<IntArray> = listOf(
      intArrayOf(1),
      intArrayOf(1, 1),
      intArrayOf(1, 2, 1),
      intArrayOf(1, 3, 3, 1),
      intArrayOf(1, 4, 6, 4, 1),
      intArrayOf(1, 5, 10, 10, 5, 1)
   )

   @Test fun `verify rows`() {
      generateTestCases().forEach {
         val row = it.size
         for (col in 1..it.size) {
            val expected = it[col - 1]
            assertEquals(expected, pascal(row, col))
         }
      }
   }
}