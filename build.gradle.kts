// SPDX-License-Identifier: LGPL-3.0-or-later

plugins {
    kotlin("jvm") version "1.3.71" apply false
}

allprojects {
    repositories {
        mavenCentral()
        jcenter()
        mavenLocal()
    }
    configurations.create("compileClasspath")
}
