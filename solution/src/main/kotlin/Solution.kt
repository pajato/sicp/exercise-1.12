package com.pajato.sicp

import java.lang.IllegalArgumentException

fun pascal(row: Int, col: Int): Int =
    when {
        row <= 0 -> throw IllegalArgumentException("Invalid row: 0 or negative")
        col <= 0 -> throw IllegalArgumentException("Invalid column: 0 or negative")
        col > row -> throw IllegalArgumentException("Invalid column: greater than the row number")
        row == 1 -> 1
        col == 1 || col == row -> 1
        else -> pascal(row - 1, col - 1) + pascal(row - 1, col)
    }
