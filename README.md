# SICP Exercise 1.12

Pascal's Triangle: the numbers at the edge of the triangle are all 1, and each number inside the triangle is the sum of the two numbers above it. Write a procedure that computes elements of Pascal’s triangle by means of a recursive process.

For example:

            1
          1   1
        1   2   1
      1   3   3   1
    1   4   6   4   1
          . . .

